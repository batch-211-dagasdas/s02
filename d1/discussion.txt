CLI (command line Interface) commands

LS- (list) the file and folder contained by the current directory 

pwd - (print working directory / present working directory) - shows the current folder we are working on

cd < folderName path to folder > - (changes the directory) change the current folder/ directory we are currntly working on our CLI( terminal/ gitbach)

sublime text 4
>>lightweight text/ file editor
>>use less ram/ mermory is important because we also , as dev have oiur google chrome open..

configure our git

git config --global user.email"ralph.dagasdas@gmail.com"
--this allows us to identify the account from gitlab or github who will push/upload file into our online gitlab or github
services"

git config --global user.name "ralph.dagasdas"
--this will allow us to dertermine the name of the person who uploaded the latest commit/version in our repo..

Pushing for the 1st time

git init
-- initialize a local folder as local git repo
-- mean that the folder and its files are now tracked by git

git add
-- it allows us to add all file and update into a new commit/version of our files and folder to be uploaded to our online repo

git commit -m "<commitMesage"
-allows us to create a new version of our files and folder based on the updates added using git add

git remote add origin <gitUrlOnlineRepo>
- this allows ut ot connect an online repo to a local repo " origin" is the default or conbertional name for an online repo

git push origin master
-- allows us to push/upload the later commit created into our online repo that is designated as "origin"
- master is the default version or th main cer


git add.
git commit -m"<commitMessage>"
git push origin master


Activity instructions:
1. In the S02 folder, create an a1 folder and an aboutme.txt file inside of it.

2. Initialize a local git repository using git init.

3. Peek at the states of the files/folders using git status.

4. Stage the files in preparation for creating a commit using git add.

5. Create a commit using git commit.

6. Check the commit history using git log/git log --oneline.

7. Add a paragraph that describes your motivation in joining the bootcamp

8. Check the status of the files/folders, stage the files and create a commit.

9. Add a paragraph that briefly narrates your work experience.

10. Check the status of the files/folders, stage the files and create a commit again.

11. In the S02 gitlab group, create a gitlab project named a1.

12. Connect to a remote repository using git remote add origin.

13. Upload your changes to the GitLab project by doing git push origin master.

14. Add the gitlab project link in Boodle.